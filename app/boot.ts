import 'angular2/bundles/angular2-polyfills';
import {provide} from 'angular2/core'
import {bootstrap}    from 'angular2/platform/browser'
import {ROUTER_PROVIDERS, LocationStrategy, HashLocationStrategy, APP_BASE_HREF} from 'angular2/router'
import {HTTP_PROVIDERS, Http} from 'angular2/http'

import {AppComponent} from './app.component'
import {NewsService} from './services/news.service'
import {UserService} from './services/user.service'
import {TodoService} from './services/todo.service'

bootstrap(AppComponent, [
	ROUTER_PROVIDERS,
	HTTP_PROVIDERS,
	provide(LocationStrategy, { useClass: HashLocationStrategy }),
	provide(APP_BASE_HREF, {useValue: '/'}),
	NewsService, 
	UserService,
	TodoService
]);