import {VisitorsComposite} from './composits/visitors/visitors.composite'
import {AdminComposite} from './composits/admin/admin.composite'

export const Routes = [
	{ path: '/', component: AdminComposite, as: 'Home' },	
	{ path: '/home/...', component: VisitorsComposite, as: 'Visitors' }	
];