import {Component} from 'angular2/core'
import {BaseComponent} from 'System'
// import * as _ from 'lodash'
// import * as $ from 'jquery'

declare var _;
declare var $;

@Component({
    template: 'Main Visitor'
})
export class VisitorsMainComponent extends BaseComponent {
	constructor(){
		super();
		console.log(_.VERSION);
		console.log( $ );
		console.log('Visitors Main component constructor');
	}
	ngOnInit(){
		console.log('Visitors Main component');
	}
}