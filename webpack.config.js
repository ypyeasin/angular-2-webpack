var webpack = require('webpack'); 

module.exports = {
  debug: true,
  cache: true,
  entry: {
    app: "./app/boot",
    vendor: ["./app/vendors"]
  },
  output: {
    path: __dirname,
    filename: "./dist/bundle.js"
  },
  devtool: 'source-map',
  resolve: {
  	extensions: ['', '.js', '.ts'],    
    alias: {         
      System: __dirname + "/app/system/System",
      lodash: 'lodash/lodash',
      jquery: 'jquery/dist/jquery.min'      
    }
  },
  plugins: [
      new webpack.ProvidePlugin({$: "jquery"}),
      new webpack.optimize.UglifyJsPlugin(),
      new webpack.optimize.CommonsChunkPlugin("vendor", "./dist/vendor.bundle.js")
  ],
  module: {
  	loaders: [
      {test: /\.ts/, loaders: ['ts-loader'], exclude: /node_modules/},
      { test: /\.css$/, loader: "style-loader!css-loader" },
      { test: /\.jpg$/, loader: "file-loader" },
      { test: /\.html$/, loader: "html" }
    ]
  },
  htmlLoader: {
    ignoreCustomFragments: [/\{\{.*?}}/]
  }
};