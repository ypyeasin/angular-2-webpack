import {Injectable} from "angular2/core"

@Injectable()
export class NewsService{		
	news = [];

	constructor(){		
		this.news = this.generateData();		
	}
	generateData(){		
		var result = [
			{ id: 1, name: 'Yeasin Hossain', email: 'yp.yeasin@gmail.com' },
			{ id: 2, name: 'Kuddus', email: 'kuddus@gmail.com' },
			{ id: 3, name: 'Boyati', email: 'boyati@gmail.com' }
		];
		return result; 
	}
}