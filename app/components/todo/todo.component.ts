import {Component} from 'angular2/core'
import {NgForm}    from 'angular2/common'
import {BaseComponent} from 'System'

import {UsersModel} from '../../models/Users.model'
import {UserService} from '../../services/user.service'
import {TodoService} from '../../services/todo.service'

@Component({
    selector: 'todo',
    templateUrl: `app/components/todo/todo.tpl.html`,    
})
export class Todo extends BaseComponent {	

	public todoForm: Object = {};
	public todoCollection = [];
	public countries = [];

	constructor(public userService: UserService, public todoService: TodoService) {
		super();		
		this.countries = todoService.getOptions();
	}


	addTodo(){
		this.todoCollection.push(this.todoForm);
		this.todoForm = {};
	}


}