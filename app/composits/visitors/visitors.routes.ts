import {Login} from './../../components/login/login.component'
import {Friends} from './../../components/friends/friends.component'
import {VisitorsMainComponent} from './component/visitors.main.component'

export var routes = [
	{ path: '/', component: VisitorsMainComponent, useAsDefault: true },
	{ path: 'products', component: Login, as: 'Home' },
	{ path: 'friends', component: Friends, as: 'Friends' }
];