import {Injectable} from "angular2/core"
import {Http, Response} from 'angular2/http'
import 'rxjs/Rx';
import {BaseService} from "./base.service"

@Injectable()
export class TodoService extends BaseService {	

	constructor(private http: Http) {				
		super();		
	}

	getOptions(){
		return [
			{id: 1, country: 'Bangladesh'},
			{id: 2, country: 'KSA'},
			{id: 3, country: 'USA'}
		];
		
	}	

}