import {Component} from 'angular2/core'
import {ROUTER_DIRECTIVES, RouteConfig, RouterOutlet} from 'angular2/router'
import {BaseComponent} from 'System'
// import {AlertComponent} from 'ng2-bootstrap/ng2-bootstrap';

import {Routes} from './routes'
import {Login} from './components/login/login.component'
import {Friends} from './components/friends/friends.component'
import {Todo} from './components/todo/todo.component'

var template = require('./app.component.tpl.html');
console.log(typeof template);

@Component({
   	selector: 'my-app',
		  directives: [ROUTER_DIRECTIVES],
    templateUrl: 'app/app.component.tpl.html'    
})
@RouteConfig(Routes)
export class AppComponent extends BaseComponent{ }