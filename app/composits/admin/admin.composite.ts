import {Component} from 'angular2/core'
import {ROUTER_DIRECTIVES} from 'angular2/router'

import {Login} from './../../components/login/login.component'

@Component({
   	selector: 'admin-composite',
  	directives: [ROUTER_DIRECTIVES, Login],
	templateUrl: 'app/composits/admin/admin.composite.tpl.html'
})
export class AdminComposite {}