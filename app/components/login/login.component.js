"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('angular2/core');
var System_1 = require('System');
var user_service_1 = require('../../services/user.service');
var Login = (function (_super) {
    __extends(Login, _super);
    function Login(userService) {
        _super.call(this);
        this.userService = userService;
        this.loginModel = {
            first_name: '', last_name: ''
        };
        var url = require("../../images/1.jpg");
    }
    Login.prototype.login = function (elem) {
        var user = this.userService.login();
        console.log(user);
    };
    Login = __decorate([
        core_1.Component({
            selector: 'login',
            templateUrl: "app/components/login/login.tpl.html"
        }), 
        __metadata('design:paramtypes', [user_service_1.UserService])
    ], Login);
    return Login;
}(System_1.BaseComponent));
exports.Login = Login;
//# sourceMappingURL=login.component.js.map