import {Component} from 'angular2/core'
import {NgForm}    from 'angular2/common'
import {BaseComponent} from 'System'

import {UsersModel} from '../../models/Users.model'
import {UserService} from '../../services/user.service'

@Component({
    selector: 'login',
    templateUrl: `app/components/login/login.tpl.html`
})
export class Login extends BaseComponent{
	public foods;
	public loginModel = {
		first_name: '', last_name: ''
	};

	constructor(public userService: UserService) {
		super();
		var url = require("../../images/1.jpg");
	}

	login(elem) {
		var user = this.userService.login();
		console.log(user);
	}
}
