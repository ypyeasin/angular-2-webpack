import {Injectable} from "angular2/core"
import {Http, Response} from 'angular2/http'
import 'rxjs/Rx';
import {BaseService} from "./base.service"

@Injectable()
export class UserService extends BaseService {	

	public user: Object = {};

	constructor(private http: Http) {				
		super();		
	}

	login(){
		return this.http.get('/api/user.json')
			.map((res: Response) => res.json())
			.do(data => console.log(data))
			.subscribe(data => console.log(data))						
	}	

}