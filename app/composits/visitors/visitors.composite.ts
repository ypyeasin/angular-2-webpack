import {Component} from 'angular2/core'
import {ROUTER_DIRECTIVES, RouteConfig, RouterOutlet} from 'angular2/router'
import {BaseComponent} from 'System'

import {Todo} from './../../components/todo/todo.component'
import {routes} from './visitors.routes'

@Component({
   	selector: 'visitors-composite',
	directives: [ROUTER_DIRECTIVES, Todo],
    templateUrl: 'app/composits/visitors/visitors.composite.tpl.html'    
})
@RouteConfig(routes)
export class VisitorsComposite extends BaseComponent{ }