import {Component} from 'angular2/core';
import {NewsService} from '../../services/news.service';
import {UserService} from '../../services/user.service'

@Component({
    selector: 'friends',    
    template: `
        <ul>
            <li *ngFor="#contact of newsService.news" 
                (click)="selected(contact)">
                {{ contact.name }}
            </li>
        </ul>
        <p>{{userService.user.first_name}}</p>
    `
})
export class Friends {    
    constructor(public newsService: NewsService, public userService: UserService) {}	
    selected(model){
        this.newsService.news.push(model);
        console.log(model);
    }
}