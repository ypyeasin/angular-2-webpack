import {Http, Response} from 'angular2/http';
import {Observable}     from 'rxjs/Observable';

export abstract class Model{
	
	public attributes: Object;
	protected url: string;

	constructor(private http: Http) { 
		console.log( http );
	}

	save( param ){
		console.log("Save", this.url);		
	}

	fetch( param ) {
		console.log("Fetch", this.url);
	}

}