import {Model} from "System"
import {Http, Response} from 'angular2/http'
import {Observable}     from 'rxjs/Observable'
import {Injectable} from "angular2/core"

@Injectable()
export class UsersModel {	
	public url: string = "test.json"
	public attributes

	constructor(private http: Http) {
		console.log(http);
	}

	save(param){
		console.log("Save", this.url);
		param = param || {};
		return this.http.get(this.url).subscribe(res => {
			this.attributes = res.json();
		});
	}
}